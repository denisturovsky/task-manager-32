package ru.tsc.denisturovsky.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.dto.request.UserLoginRequest;
import ru.tsc.denisturovsky.tm.dto.request.UserLogoutRequest;
import ru.tsc.denisturovsky.tm.dto.request.UserViewProfileRequest;
import ru.tsc.denisturovsky.tm.dto.response.UserLoginResponse;
import ru.tsc.denisturovsky.tm.dto.response.UserLogoutResponse;
import ru.tsc.denisturovsky.tm.dto.response.UserViewProfileResponse;

public interface IAuthEndpoint {

    @NotNull
    UserLoginResponse userLogin(@NotNull UserLoginRequest request);

    @NotNull
    UserLogoutResponse userLogout(@NotNull UserLogoutRequest request);

    @NotNull
    UserViewProfileResponse userViewProfile(@NotNull UserViewProfileRequest request);

}
