package ru.tsc.denisturovsky.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.dto.request.*;
import ru.tsc.denisturovsky.tm.dto.response.*;

public interface IDomainEndpoint {

    @NotNull
    DataBackupLoadResponse dataBackupLoad(@NotNull DataBackupLoadRequest request);

    @NotNull
    DataBackupSaveResponse dataBackupSave(@NotNull DataBackupSaveRequest request);

    @NotNull
    DataBase64LoadResponse dataBase64Load(@NotNull DataBase64LoadRequest request);

    @NotNull
    DataBase64SaveResponse dataBase64Save(@NotNull DataBase64SaveRequest request);

    @NotNull
    DataBinaryLoadResponse dataBinaryLoad(@NotNull DataBinaryLoadRequest request);

    @NotNull
    DataBinarySaveResponse dataBinarySave(@NotNull DataBinarySaveRequest request);

    @NotNull
    DataJsonLoadFasterXmlResponse dataJsonLoadFasterXml(@NotNull DataJsonLoadFasterXmlRequest request);

    @NotNull
    DataJsonLoadJaxBResponse dataJsonLoadJaxB(@NotNull DataJsonLoadJaxBRequest request);

    @NotNull
    DataJsonSaveFasterXmlResponse dataJsonSaveFasterXml(@NotNull DataJsonSaveFasterXmlRequest request);

    @NotNull
    DataJsonSaveJaxBResponse dataJsonSaveJaxB(@NotNull DataJsonSaveJaxBRequest request);

    @NotNull
    DataXmlLoadFasterXmlResponse dataXmlLoadFasterXml(@NotNull DataXmlLoadFasterXmlRequest request);

    @NotNull
    DataXmlLoadJaxBResponse dataXmlLoadJaxB(@NotNull DataXmlLoadJaxBRequest request);

    @NotNull
    DataXmlSaveFasterXmlResponse dataXmlSaveFasterXml(@NotNull DataXmlSaveFasterXmlRequest request);

    @NotNull
    DataXmlSaveJaxBResponse dataXmlSaveJaxB(@NotNull DataXmlSaveJaxBRequest request);

    @NotNull
    DataYamlLoadFasterXmlResponse dataYamlLoadFasterXml(@NotNull DataYamlLoadFasterXmlRequest request);

    @NotNull
    DataYamlSaveFasterXmlResponse dataYamlSaveFasterXml(@NotNull DataYamlSaveFasterXmlRequest request);

}
