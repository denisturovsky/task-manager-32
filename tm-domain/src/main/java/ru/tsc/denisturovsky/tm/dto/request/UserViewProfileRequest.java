package ru.tsc.denisturovsky.tm.dto.request;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class UserViewProfileRequest extends AbstractUserRequest {

}
