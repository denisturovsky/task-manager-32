package ru.tsc.denisturovsky.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.dto.request.ApplicationAboutRequest;
import ru.tsc.denisturovsky.tm.dto.request.ApplicationVersionRequest;
import ru.tsc.denisturovsky.tm.dto.response.ApplicationAboutResponse;
import ru.tsc.denisturovsky.tm.dto.response.ApplicationVersionResponse;

public interface ISystemEndpoint {

    @NotNull ApplicationAboutResponse applicationAbout(@NotNull ApplicationAboutRequest request);

    @NotNull ApplicationVersionResponse applicationVersion(@NotNull ApplicationVersionRequest request);

}
