package ru.tsc.denisturovsky.tm.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class ApplicationAboutResponse extends AbstractResponse {

    private String email;

    private String name;
    
}
