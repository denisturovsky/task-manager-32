package ru.tsc.denisturovsky.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.dto.request.DataBinarySaveRequest;

public final class DataBinarySaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String DESCRIPTION = "Save data in binary file.";

    @NotNull
    public static final String NAME = "data-save-bin";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA BINARY SAVE]");
        @NotNull final DataBinarySaveRequest request = new DataBinarySaveRequest();
        getDomainEndpointClient().dataBinarySave(request);
    }

}
