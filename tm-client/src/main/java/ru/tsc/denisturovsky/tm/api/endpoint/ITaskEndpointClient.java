package ru.tsc.denisturovsky.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;

import java.net.Socket;

public interface ITaskEndpointClient extends ITaskEndpoint {

    void setSocket(@Nullable Socket socket);

}
