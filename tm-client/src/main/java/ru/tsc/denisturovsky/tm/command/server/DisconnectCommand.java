package ru.tsc.denisturovsky.tm.command.server;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.command.AbstractCommand;
import ru.tsc.denisturovsky.tm.enumerated.Role;

public final class DisconnectCommand extends AbstractCommand {

    @NotNull
    public static final String DESCRIPTION = "Disconnect.";

    @NotNull
    public static final String NAME = "disonnect";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return NAME;
    }

    @Nullable
    public Role[] getRoles() {
        return null;
    }

    @Override
    @SneakyThrows
    public void execute() {
        try {
            assert getServiceLocator() != null;
            getServiceLocator().getConnectionEndpointClient().disconnect();
        } catch (@NotNull final Exception e) {
            assert getServiceLocator() != null;
            getServiceLocator().getLoggerService().error(e);
        }
    }

}
