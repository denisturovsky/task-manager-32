package ru.tsc.denisturovsky.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.dto.request.TaskCompleteByIdRequest;
import ru.tsc.denisturovsky.tm.util.TerminalUtil;

public final class TaskCompleteByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-complete-by-id";

    @NotNull
    public static final String DESCRIPTION = "Complete task by id";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest();
        request.setId(id);
        getTaskEndpoint().taskCompleteById(request);
    }

}
