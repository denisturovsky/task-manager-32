package ru.tsc.denisturovsky.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.api.endpoint.IAuthEndpointClient;
import ru.tsc.denisturovsky.tm.dto.request.UserLoginRequest;
import ru.tsc.denisturovsky.tm.dto.request.UserLogoutRequest;
import ru.tsc.denisturovsky.tm.dto.request.UserViewProfileRequest;
import ru.tsc.denisturovsky.tm.dto.response.UserLoginResponse;
import ru.tsc.denisturovsky.tm.dto.response.UserLogoutResponse;
import ru.tsc.denisturovsky.tm.dto.response.UserViewProfileResponse;

@NoArgsConstructor
public final class AuthEndpointClient extends AbstractEndpointClient implements IAuthEndpointClient {

    public AuthEndpointClient(@NotNull final AbstractEndpointClient client) {
        super(client);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
        authEndpointClient.connect();
        System.out.println(authEndpointClient.userLogin(new UserLoginRequest("ADMIN", "ADMIN")));
        System.out.println(authEndpointClient.userViewProfile(new UserViewProfileRequest()).getUser());
        System.out.println(authEndpointClient.userLogout(new UserLogoutRequest()));
        authEndpointClient.disconnect();
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLoginResponse userLogin(@NotNull UserLoginRequest request) {
        return call(request, UserLoginResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLogoutResponse userLogout(@NotNull UserLogoutRequest request) {
        return call(request, UserLogoutResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserViewProfileResponse userViewProfile(@NotNull UserViewProfileRequest request) {
        return call(request, UserViewProfileResponse.class);
    }

}
