package ru.tsc.denisturovsky.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.dto.request.DataJsonSaveFasterXmlRequest;

public final class DataJsonSaveFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String DESCRIPTION = "Save data in json file.";

    @NotNull
    public static final String NAME = "data-save-json";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE JSON]");
        @NotNull final DataJsonSaveFasterXmlRequest request = new DataJsonSaveFasterXmlRequest();
        getDomainEndpointClient().dataJsonSaveFasterXml(request);
    }

}
