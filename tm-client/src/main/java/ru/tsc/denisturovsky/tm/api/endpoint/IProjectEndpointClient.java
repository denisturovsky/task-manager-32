package ru.tsc.denisturovsky.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;

import java.net.Socket;

public interface IProjectEndpointClient extends IProjectEndpoint {

    void setSocket(@Nullable Socket socket);

}
