package ru.tsc.denisturovsky.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.api.endpoint.ISystemEndpointClient;
import ru.tsc.denisturovsky.tm.dto.request.ApplicationAboutRequest;
import ru.tsc.denisturovsky.tm.dto.request.ApplicationVersionRequest;
import ru.tsc.denisturovsky.tm.dto.response.ApplicationAboutResponse;
import ru.tsc.denisturovsky.tm.dto.response.ApplicationVersionResponse;

public final class SystemEndpointClient extends AbstractEndpointClient implements ISystemEndpointClient {

    @SneakyThrows
    public static void main(String[] args) {
        final SystemEndpointClient client = new SystemEndpointClient();
        client.connect();
        final ApplicationAboutResponse serverAboutResponse = client.applicationAbout(new ApplicationAboutRequest());
        System.out.println(serverAboutResponse.getEmail());
        System.out.println(serverAboutResponse.getName());
        final ApplicationVersionResponse serverVersionResponse = client.applicationVersion(new ApplicationVersionRequest());
        System.out.println(serverVersionResponse.getVersion());
        client.disconnect();
    }

    @NotNull
    @Override
    @SneakyThrows
    public ApplicationAboutResponse applicationAbout(@NotNull final ApplicationAboutRequest request) {
        return call(request, ApplicationAboutResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ApplicationVersionResponse applicationVersion(@NotNull final ApplicationVersionRequest request) {
        return call(request, ApplicationVersionResponse.class);
    }

}
