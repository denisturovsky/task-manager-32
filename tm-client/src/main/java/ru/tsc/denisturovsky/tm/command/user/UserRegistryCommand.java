package ru.tsc.denisturovsky.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.dto.request.UserRegistryRequest;
import ru.tsc.denisturovsky.tm.enumerated.Role;
import ru.tsc.denisturovsky.tm.model.User;
import ru.tsc.denisturovsky.tm.util.TerminalUtil;

public final class UserRegistryCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-registry";

    @NotNull
    public static final String DESCRIPTION = "Registry new user";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("[ENTER LOGIN:]");
        @NotNull String login = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD:]");
        @NotNull String password = TerminalUtil.nextLine();
        System.out.println("[ENTER EMAIL:]");
        @NotNull String email = TerminalUtil.nextLine();
        @NotNull final UserRegistryRequest request = new UserRegistryRequest();
        request.setLogin(login);
        request.setPassword(password);
        request.setEmail(email);
        @Nullable final User user = getUserEndpoint().userRegistry(request).getUser();
        showUser(user);
    }

}
