package ru.tsc.denisturovsky.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;

import java.net.Socket;

public interface IAuthEndpointClient extends IAuthEndpoint {

    void setSocket(@Nullable Socket socket);

}
