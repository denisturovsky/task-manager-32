package ru.tsc.denisturovsky.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.dto.request.DataBinaryLoadRequest;

public final class DataBinaryLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String DESCRIPTION = "Load data from binary file.";

    @NotNull
    public static final String NAME = "data-load-bin";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA BINARY LOAD]");
        @NotNull final DataBinaryLoadRequest request = new DataBinaryLoadRequest();
        getDomainEndpointClient().dataBinaryLoad(request);
    }

}
