package ru.tsc.denisturovsky.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;

import java.net.Socket;

public interface IUserEndpointClient extends IUserEndpoint {

    void setSocket(@Nullable Socket socket);

}
