package ru.tsc.denisturovsky.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.api.endpoint.ISystemEndpoint;
import ru.tsc.denisturovsky.tm.api.service.IPropertyService;
import ru.tsc.denisturovsky.tm.api.service.IServiceLocator;
import ru.tsc.denisturovsky.tm.dto.request.ApplicationAboutRequest;
import ru.tsc.denisturovsky.tm.dto.request.ApplicationVersionRequest;
import ru.tsc.denisturovsky.tm.dto.response.ApplicationAboutResponse;
import ru.tsc.denisturovsky.tm.dto.response.ApplicationVersionResponse;

public final class SystemEndpoint extends AbstractEndpoint implements ISystemEndpoint {

    public SystemEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    public ApplicationAboutResponse applicationAbout(@NotNull ApplicationAboutRequest request) {
        @NotNull final IPropertyService propertyService = getServiceLocator().getPropertyService();
        @NotNull ApplicationAboutResponse response = new ApplicationAboutResponse();
        response.setEmail(propertyService.getAuthorEmail());
        response.setName(propertyService.getAuthorName());
        return response;
    }

    @NotNull
    @Override
    public ApplicationVersionResponse applicationVersion(@NotNull ApplicationVersionRequest request) {
        @NotNull final IPropertyService propertyService = getServiceLocator().getPropertyService();
        @NotNull ApplicationVersionResponse response = new ApplicationVersionResponse();
        response.setVersion(propertyService.getApplicationVersion());
        return response;
    }

}
