package ru.tsc.denisturovsky.tm.task;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.api.service.IAuthService;
import ru.tsc.denisturovsky.tm.api.service.IUserService;
import ru.tsc.denisturovsky.tm.component.Server;
import ru.tsc.denisturovsky.tm.dto.request.*;
import ru.tsc.denisturovsky.tm.dto.response.*;
import ru.tsc.denisturovsky.tm.model.User;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;

@Getter
@Setter
public final class ServerRequestTask extends AbstractServerSocketTask {

    @Nullable
    private AbstractResponse response;
    @Nullable
    private AbstractRequest request;

    public ServerRequestTask(
            @NotNull final Server server,
            @NotNull final Socket socket
    ) {
        super(server, socket);
    }

    public ServerRequestTask(
            @NotNull final Server server,
            @NotNull final Socket socket,
            @Nullable final String userId
    ) {
        super(server, socket, userId);
    }

    @Override
    @SneakyThrows
    public void run() {
        processInput();
        processUserId();
        processLogin();
        processProfile();
        processLogout();
        processOperation();
        processOutput();
    }

    private void processUserId() {
        if (!(request instanceof AbstractUserRequest)) return;
        @NotNull final AbstractUserRequest abstractUserRequest = (AbstractUserRequest) request;
        abstractUserRequest.setUserId(userId);
    }

    @SneakyThrows
    private void processInput() {
        @NotNull final InputStream inputStream = socket.getInputStream();
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
        @NotNull final Object object = objectInputStream.readObject();
        request = (AbstractRequest) object;
    }

    @SneakyThrows
    private void processOutput() {
        @NotNull final OutputStream outputStream = socket.getOutputStream();
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(response);
        server.submit(new ServerRequestTask(server, socket, userId));
    }

    private void processLogin() {
        if (response != null) return;
        if (!(request instanceof UserLoginRequest)) return;
        try {
            @NotNull final UserLoginRequest userLoginRequest = (UserLoginRequest) request;
            @Nullable final String login = userLoginRequest.getLogin();
            @Nullable final String password = userLoginRequest.getPassword();
            @NotNull final IAuthService authService = server.getBootstrap().getAuthService();
            @NotNull final User user = authService.check(login, password);
            userId = user.getId();
            response = new UserLoginResponse();
        } catch (@NotNull final Exception e) {
            response = new UserLoginResponse(e);
        }
    }

    private void processProfile() {
        if (response != null) return;
        if (!(request instanceof UserViewProfileRequest)) return;
        if (userId == null) {
            response = new UserViewProfileResponse();
            return;
        }
        @NotNull final IUserService userService = server.getBootstrap().getUserService();
        @Nullable final User user = userService.findOneById(userId);
        response = new UserViewProfileResponse(user);
    }

    private void processLogout() {
        if (response != null) return;
        if (!(request instanceof UserLogoutRequest)) return;
        userId = null;
        response = new UserLogoutResponse();
    }

    private void processOperation() {
        if (response != null) return;
        try {
            @Nullable final Object result = server.call(request);
            response = (AbstractResponse) result;
        } catch (@NotNull final Exception e) {
            response = new ApplicationErrorResponse(e);
        }
    }

}