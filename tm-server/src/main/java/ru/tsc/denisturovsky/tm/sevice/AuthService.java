package ru.tsc.denisturovsky.tm.sevice;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.api.service.IAuthService;
import ru.tsc.denisturovsky.tm.api.service.IPropertyService;
import ru.tsc.denisturovsky.tm.api.service.IUserService;
import ru.tsc.denisturovsky.tm.exception.field.LoginEmptyException;
import ru.tsc.denisturovsky.tm.exception.field.PasswordEmptyException;
import ru.tsc.denisturovsky.tm.exception.user.LoginPasswordIncorrectException;
import ru.tsc.denisturovsky.tm.exception.user.UserLockedException;
import ru.tsc.denisturovsky.tm.model.User;
import ru.tsc.denisturovsky.tm.util.HashUtil;

public class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    public AuthService(
            @NotNull IPropertyService propertyService,
            @NotNull IUserService userService
    ) {
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public User registry(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        return userService.create(login, password, email);
    }

    @NotNull
    @Override
    public User check(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new LoginPasswordIncorrectException();
        if (user.getLocked()) throw new UserLockedException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null) throw new LoginPasswordIncorrectException();
        if (!hash.equals(user.getPasswordHash())) throw new LoginPasswordIncorrectException();
        return user;
    }

}