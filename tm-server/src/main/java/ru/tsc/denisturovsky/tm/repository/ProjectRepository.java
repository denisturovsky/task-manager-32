package ru.tsc.denisturovsky.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.api.repository.IProjectRepository;
import ru.tsc.denisturovsky.tm.model.Project;

import java.util.Objects;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @NotNull
    @Override
    public Project create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) {
        @NotNull final Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        return Objects.requireNonNull(add(project));
    }

    @NotNull
    @Override
    public Project create(
            @NotNull final String userId,
            @NotNull final String name
    ) {
        @NotNull final Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        return Objects.requireNonNull(add(project));
    }

}
